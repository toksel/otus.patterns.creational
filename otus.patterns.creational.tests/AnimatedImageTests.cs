﻿using NUnit.Framework;
using otus.patterns.creational.UIElements;
using System.Drawing;

namespace otus.patterns.creational.tests
{
   class AnimatedImageTests
   {
      static AnimatedImage baseElement = new()
      {
         Color = Color.Aqua,
         Code = "baseElement",
         HierarchyLevel = 0,
         ElementVisible = true,
         NsfwContent = true,
         Resolution = (1920, 1080),
         DurationSeconds = 15,
         AnimationSpeed = 10.0f
      };

      [Test]
      public void CheckCopyIsTheSameType()
      {
         var testElement = baseElement.Clone();
         Assert.AreEqual(typeof(AnimatedImage), testElement.GetType());
      }

      [Test]
      public void CheckCopyIsNotTheSameReference()
      {
         var testElement = baseElement.Clone();
         Assert.IsFalse(ReferenceEquals(baseElement, testElement));
      }

      [Test]
      public void CheckCopyIsIndependent()
      {
         var testElement = baseElement.Clone();
         baseElement.DurationSeconds = 10;
         Assert.AreEqual(15, testElement.DurationSeconds);
      }
   }
}
