﻿using NUnit.Framework;
using otus.patterns.creational.UIElements;
using System;
using System.Drawing;

namespace otus.patterns.creational.tests
{
   class GalleryElementTests
   {
      static GalleryElement baseElement = new()
      {
         Color = Color.Aqua,
         Code = "baseElement",
         HierarchyLevel = 0,
         ElementVisible = true,
         NsfwContent = true,
         Resolution = (1920, 1080)
      };

      [Test]
      public void CheckCopyIsTheSameType()
      {
         var testElement = baseElement.Clone();
         Assert.AreEqual(typeof(GalleryElement), testElement.GetType());
      }

      [Test]
      public void CheckCopyIsNotTheSameReference()
      {
         var testElement = baseElement.Clone();
         Assert.IsFalse(ReferenceEquals(baseElement, testElement));
      }

      [Test]
      public void CheckCopyIsIndependent()
      {
         var testElement = baseElement.Clone();
         baseElement.Resolution = (1024, 768);
         (int x, int y) res = testElement.Resolution;
         Assert.AreEqual((1920, 1080), res);
      }

      [Test]
      public void CheckCopyHasUpvotes()
      {
         baseElement.ResetUpvotes();
         baseElement.Upvote();
         baseElement.Upvote();
         baseElement.Upvote();
         var testElement = baseElement.Clone();
         Assert.AreEqual(3, testElement.Upvotes);
      }
   }
}
