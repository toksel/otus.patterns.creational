﻿using NUnit.Framework;
using otus.patterns.creational.UIElements;
using System;
using System.Drawing;
using System.Globalization;

namespace otus.patterns.creational.tests
{
   class PhotoTests
   {
      static Photo baseElement = new()
      {
         Color = Color.Aqua,
         Code = "baseElement",
         HierarchyLevel = 0,
         ElementVisible = true,
         NsfwContent = true,
         Resolution = (1920, 1080),
         CaptureDate = new DateTime(2021, 6, 23, 1, 0, 0),
         CapturePlace = new() { Country = "Russia", City = "St Petersbur", AddressLine = "Unknown"}
      };

      [Test]
      public void CheckCopyIsTheSameType()
      {
         var testElement = baseElement.Clone();
         Assert.AreEqual(typeof(Photo), testElement.GetType());
      }

      [Test]
      public void CheckCopyIsNotTheSameReference()
      {
         var testElement = baseElement.Clone();
         Assert.IsFalse(ReferenceEquals(baseElement, testElement));
      }

      [Test]
      public void CheckCopyIsIndependent()
      {
         var testElement = baseElement.Clone();
         baseElement.CaptureDate = DateTime.Now;
         Assert.AreEqual(DateTime.ParseExact("2021-06-23 01:00:00", "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture), testElement.CaptureDate);
      }
   }
}
