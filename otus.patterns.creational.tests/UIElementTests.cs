﻿using NUnit.Framework;
using otus.patterns.creational.UIElements;
using System.Drawing;

namespace otus.patterns.creational.tests
{
   class UIElementTests
   {
      static UIElement baseElement = new()
      {
         Color = Color.Aqua,
         Code = "baseElement",
         HierarchyLevel = 0
      };

      [Test]
      public void CheckCopyIsTheSameType()
      {
         var testElement = baseElement.Clone();
         Assert.AreEqual(typeof(UIElement), testElement.GetType());
      }

      [Test]
      public void CheckCopyIsNotTheSameReference()
      {
         var testElement = baseElement.Clone();
         Assert.IsFalse(ReferenceEquals(baseElement, testElement));
      }

      [Test]
      public void CheckCopyIsIndependent()
      {
         var testElement = baseElement.Clone();
         baseElement.HierarchyLevel = 1;
         Assert.AreEqual(0, testElement.HierarchyLevel);
      }
   }
}
