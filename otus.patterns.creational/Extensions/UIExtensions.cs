﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace otus.patterns.creational.Extensions
{
   public static class UIExtensions
   {
      private static readonly MethodInfo CloneMethod = typeof(Object).GetMethod("MemberwiseClone", BindingFlags.NonPublic | BindingFlags.Instance);

      public static bool IsPrimitive(this Type type)
         => type == typeof(string) ||
            (type.IsValueType && type.IsPrimitive);

      public static T Copy<T>(this T originalObject)
         => InternalCopy(originalObject, new Dictionary<object, object>(new ReferenceEqualityComparer()));

      private static T InternalCopy<T>(T originalObject, IDictionary<object, object> visited)
      {
         if (originalObject == null)
            return default;
         var typeToReflect = originalObject.GetType();
         if (IsPrimitive(typeToReflect))
            return originalObject;
         if (visited.ContainsKey(originalObject))
            return (T)visited[originalObject];
         if (typeof(Delegate).IsAssignableFrom(typeToReflect))
            return default;
         var cloneObject = CloneMethod.Invoke(originalObject, null);
         if (typeToReflect.IsArray)
         {
            var arrayType = typeToReflect.GetElementType();
            if (IsPrimitive(arrayType) == false)
            {
               Array clonedArray = (Array)cloneObject;
               clonedArray.ForEach((array, indices) => array.SetValue(InternalCopy(clonedArray.GetValue(indices), visited), indices));
            }
         }
         visited.Add(originalObject, cloneObject);
         CopyFields(originalObject, visited, cloneObject, typeToReflect);
         RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect);
         return (T)cloneObject;
      }

      private static void RecursiveCopyBaseTypePrivateFields(object originalObject, IDictionary<object, object> visited, object cloneObject, Type typeToReflect)
      {
         if (typeToReflect.BaseType != null)
         {
            RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect.BaseType);
            CopyFields(originalObject, visited, cloneObject, typeToReflect.BaseType, BindingFlags.Instance | BindingFlags.NonPublic, info => info.IsPrivate);
         }
      }

      private static void CopyFields(object originalObject, IDictionary<object, object> visited, object cloneObject, Type typeToReflect, BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy, Func<FieldInfo, bool> filter = null)
      {
         foreach (FieldInfo fieldInfo in typeToReflect.GetFields(bindingFlags))
         {
            if (filter != null && filter(fieldInfo) == false)
               continue;
            if (IsPrimitive(fieldInfo.FieldType))
               continue;
            var originalFieldValue = fieldInfo.GetValue(originalObject);
            var clonedFieldValue = InternalCopy(originalFieldValue, visited);
            fieldInfo.SetValue(cloneObject, clonedFieldValue);
         }
      }
   }
}
