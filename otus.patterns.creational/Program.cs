﻿using otus.patterns.creational.UIElements;
using System;
using System.Drawing;

namespace otus.patterns.creational
{
   class Program
   {
      static void Main(string[] args)
      {
         GalleryElement element = new()
         {
            Color = Color.Aqua,
            Code = "element1",
            HierarchyLevel = 0,
            ElementVisible = true,
            NsfwContent = true,
            Resolution = (1920, 1080)
         };

         var copy = element.Clone();
         Print(element, copy);

         Console.WriteLine();
         Console.WriteLine("Let's make some changes on initial element and check that copy hasn't been changed (resolution, 2x upves)");

         element.Upvote();
         element.Upvote();
         element.Resolution = (1024, 768);
         Print(element, copy);

         Console.ReadKey();
      }

      static void Print(GalleryElement initial, GalleryElement copy)
      {
         Console.WriteLine("Initial element:");
         Console.WriteLine(initial);
         Console.WriteLine("Copied element:");
         Console.WriteLine(copy);
      }
   }
}
