﻿using System;

namespace otus.patterns.creational.UIElements
{
   public struct Address
   {
      public string Country { get; set; }
      public string City { get; set; }
      public string AddressLine { get; set; }
   }
}
