﻿using System;

namespace otus.patterns.creational.UIElements
{
   public class AnimatedImage : GalleryElement, IMyCloneable<AnimatedImage>, ICloneable
   {
      public int DurationSeconds { get; set; }
      public float AnimationSpeed { get; set; }

      public override AnimatedImage Clone()
      {
         var ret = (AnimatedImage)base.Clone();
         ret.DurationSeconds = DurationSeconds;
         ret.AnimationSpeed = AnimationSpeed;
         return ret;
      }
      object ICloneable.Clone() => Clone();
   }
}
