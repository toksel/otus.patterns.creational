﻿using System;

namespace otus.patterns.creational.UIElements
{
   public class GalleryElement : UIElement, IMyCloneable<GalleryElement>, ICloneable
   {
      int _upvotes;
      public string Title { get; set; }
      public string Description { get; set; }
      public bool ElementVisible { get; set; }
      public Resolution Resolution { get; set; }
      public bool NsfwContent { get; set; }
      public override string ToString()
         => $"Visible: {ElementVisible}; NSFW: {NsfwContent}; Resolution: {Resolution}; Upvotes {Upvotes}";

      public void Upvote() => _upvotes++;

      public int Upvotes => _upvotes;

      public void ResetUpvotes()
      {
         _upvotes = 0;
      }

      public override GalleryElement Clone()
      {
         GalleryElement ret = (GalleryElement)base.Clone();
         ret._upvotes = _upvotes;
         ret.Title = Title;
         ret.Description = Description;
         ret.Resolution = Resolution;
         ret.NsfwContent = NsfwContent;
         return ret;
      }
      object ICloneable.Clone() => Clone();
   }
}
