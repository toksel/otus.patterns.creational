﻿namespace otus.patterns.creational.UIElements
{
   public interface IMyCloneable<T>
   {
      T Clone();
   }
}
