﻿using System;

namespace otus.patterns.creational.UIElements
{
   public class Photo : GalleryElement, IMyCloneable<Photo>, ICloneable
   {
      public DateTime CaptureDate { get; set; }
      public Address CapturePlace { get; set; }

      public override Photo Clone()
      {
         var ret = (Photo)base.Clone();
         ret.CaptureDate = CaptureDate;
         ret.CapturePlace = CapturePlace;
         return ret;
      }
      object ICloneable.Clone() => Clone();
   }
}
