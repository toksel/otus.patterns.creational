﻿using System;

namespace otus.patterns.creational.UIElements
{
   public struct Resolution
   {
      public int ResolutionX { get; set; }
      public int ResolutionY { get; set; }
      public Resolution((int x, int y)resolution)
      {
         ResolutionX = resolution.x;
         ResolutionY = resolution.y;
      }

      public static implicit operator Resolution((int x, int y) resolution)
         => new(resolution);

      public static implicit operator (int x, int y)(Resolution resolution)
         => (resolution.ResolutionX, resolution.ResolutionY);

      public override string ToString() => $"({ResolutionX}:{ResolutionY})";
   }
}
