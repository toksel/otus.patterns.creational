﻿using otus.patterns.creational.Extensions;
using System;
using System.Drawing;

namespace otus.patterns.creational.UIElements
{
   public class UIElement : IMyCloneable<UIElement>, ICloneable
   {
      public Color Color { get; set; }
      public string Code { get; set; }
      public int HierarchyLevel { get; set; }
      public virtual UIElement Clone() => this.Copy();
      public override string ToString()
         => $"Code: {Code}; Color: {Color}; Hiearchy: {HierarchyLevel}";
      object ICloneable.Clone() => this.Copy();
   }
}
